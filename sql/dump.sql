CREATE TABLE "clients" (
	"id" serial NOT NULL,
	"first_name" VARCHAR(50) NOT NULL UNIQUE,
	"second_name" VARCHAR(50) NOT NULL,
	CONSTRAINT "clients_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "products" (
	"id" serial NOT NULL,
	"name" VARCHAR(50) NOT NULL UNIQUE,
	"price" FLOAT NOT NULL,
	CONSTRAINT "products_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);



CREATE TABLE "checks" (
	"id" serial NOT NULL,
	"client_id" integer NOT NULL,
	"product_id" integer NOT NULL,
	"created_at" TIMESTAMP NOT NULL,
	CONSTRAINT "checks_pk" PRIMARY KEY ("id")
) WITH (
  OIDS=FALSE
);





ALTER TABLE "checks" ADD CONSTRAINT "checks_fk0" FOREIGN KEY ("client_id") REFERENCES "clients"("id");
ALTER TABLE "checks" ADD CONSTRAINT "checks_fk1" FOREIGN KEY ("product_id") REFERENCES "products"("id");
